# Papers citing MOKIT

1. On the origin of reactivity variation upon sequential ligation: the [Re(Cl)<sub>x</sub>]<sup>+</sup>/CH<sub>4</sub> (*x*=1−3) couples. ***Phys Chem Chem Phys*** DOI: [10.1039/d1cp03468e](https://doi.org/10.1039/D1CP03468E)

2. Striking Size and Doping Effects of Ti−Si−O Clusters on Methane Conversion Reactions. ***Chem Eur J*** DOI: [10.1002/chem.202201136](https://doi.org/10.1002/chem.202201136)

3. Energy Decomposition Analysis of the Nature of Coordination Bonding at the Heme Iron Center in Cytochrome P450 Inhibition. ***Chem Asian J*** DOI: [10.1002/asia.202200360](https://doi.org/10.1002/asia.202200360)

4. Ab Initio Methods in First-Row Transition Metal Chemistry. ***Eur J Inorg Chem*** DOI: [10.1002/ejic.202200014](https://doi.org/10.1002/ejic.202200014)

5. The Bonding Nature of Fe−CO Complexes in Heme Proteins. ***Inorg Chem*** DOI: [10.1021/acs.inorgchem.2c02387](https://doi.org/10.1021/acs.inorgchem.2c02387)

6. Efficient Implementation of Block-Correlated Coupled Cluster Theory Based on the Generalized Valence Bond Reference for Strongly Correlated Systems. ***J Chem Theory Comput*** DOI: [10.1021/acs.jctc.2c00445](https://doi.org/10.1021/acs.jctc.2c00445)

7. On the distinct reactivity of two isomers of [IrC<sub>4</sub>H<sub>2</sub>]<sup>+</sup> toward methane and water. ***Sci China Chem*** DOI: [10.1007/s11426-022-1342-4](https://link.springer.com/article/10.1007/s11426-022-1342-4)

8. The Pincer Ligand Supported Ruthenium Catalysts for Acetylene Hydrochlorination: Molecular Mechanisms from Theoretical Insights. ***Catalysts*** DOI: [10.3390/catal13010031](https://doi.org/10.3390/catal13010031)

9. Oxygen-containing functional groups enhance uranium adsorption by aged polystyrene microplastics: Experimental and theoretical perspectives. ***Chem Eng J*** DOI: [10.1016/j.cej.2023.142730](https://doi.org/10.1016/j.cej.2023.142730)

10. Accurate rate constants for barrierless dissociation of ethanol: VRC-VTST and SS-QRRK calculations with the cheaper DFT method. ***Chem Phys Lett*** DOI: [10.1016/j.cplett.2023.140522](https://doi.org/10.1016/j.cplett.2023.140522)

11. Assessment of DFT functionals for a minimal nitrogenase [Fe(SH)<sub>4</sub>H]<sup>-</sup> model employing state-of-the-art ab initio methods. ***J Chem Phys*** DOI: [10.1063/5.0152611](https://doi.org/10.1063/5.0152611)

12. Equation-of-Motion Block-Correlated Coupled Cluster Method for Excited Electronic States of Strongly Correlated Systems. ***J Phys Chem Lett*** DOI: [10.1021/acs.jpclett.3c01474](https://doi.org/10.1021/acs.jpclett.3c01474)

