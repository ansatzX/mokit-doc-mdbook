# MOKIT Doc

for [MOKIT](https://gitlab.com/jxzou/mokit) version 1.2.6

MOKIT developers

Aug 17, 2023

<br>

**Contents**

- [Introduction](./chap1_intro.md) and [how-to-cite](./chap1-2.md)
- [Installation](./chap2_install.md)
- [Quick Start](./chap3_quick.md)
- [User's Guide](./chap4_guide.md): syntax rule, keywords, utilities, and APIs
- [Examples](./chap5_example.md)
- [Developer's Guide](./chap6.md)
- [Papers citing MOKIT](./citing.md)
- [Appendix](./chap_appdx.md): FAQs, limitations and [bug report](./chap_appdx.md#a3-bug-report)

**Hot Topics**

[How to perform GKS-EDA/SAPT](./chap5-3.md) &nbsp;&nbsp;&nbsp; [How to transfer MOs](./chap4-5.md) &nbsp;&nbsp;&nbsp; [FAQs](./chap_appdx.md#a1-frequently-asked-questions-faq)

**Tips**

You can use the &#128269; tool in the left upper corner of this page to search the whole manual, or simply press `s` on your keyboard.

