# v1.2.6
* documentation added
  - SA-CASSCF
  - lazy import
  - mirror_wfn
* updated
  - autosr example
  - keywords: GVB_conv and FcGVB
  - utilities: mkl2gjf, qchem and fch2qchem
  - pre-built info
* style
  - new flowchart for utilities

# v1.2.5
First version after documentation moved to mdbook.

* documentation added
  - pre-builts and conda releases
  - more examples
  - papers citing mokit
  - developers' guide
  - outlines for keywords, faqs
* style
  - nord theme
  - better tables
